// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CoreTypes.h"
#include "BaseNode.h"
#include "BaseMeshPositionNode.h"

class BaseMeshPositionNode;
/**
 * 
 */
class BaseLineNode : public BaseNode
{
private:
	Color color;
	float Thickness = 5.;
	BaseMeshPositionNode* NodeStart = nullptr;
	BaseMeshPositionNode* NodeEnd = nullptr;
	FTransform spawnTransform;
protected:
	virtual TSubclassOf<AGraphLineNode> GetNode();

	virtual AGraphLineNode* SpawnActor();
	void FinishSpawnActor(AGraphLineNode* graphLineNode);

public:
	BaseLineNode();
	~BaseLineNode();

	void Spawn() override;
	void Delete() override;

	void SetNodes(BaseMeshPositionNode* NewNodeStart, BaseMeshPositionNode* NewNodeEnd);

	BaseMeshPositionNode* GetStartNode();
	BaseMeshPositionNode* GetEndNode();

    Color GetColor();
	void SetColor(Color newColor);
	void SetThickness(float newThickness);
};
