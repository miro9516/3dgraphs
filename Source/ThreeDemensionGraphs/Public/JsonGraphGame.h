// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraphGameInstance.h"
#include "CoreTypes.h"
#include <string>
#include <unordered_map>
#include "BaseMeshPositionNode.h"
#include "SphereNode.h"
#include <list>

/**
 * 
 */

const float TICK_TIME = 50;
const float MAX_NUM = 100;


class JsonGraphGame : public GraphGameInstance
{
private:
	std::unordered_map<std::string, BaseMeshPositionNode *>* IdToNodeMapping;
	std::unordered_map<std::string, BaseLineNode *>* IdToLineMapping;

	TArray<std::unordered_map<std::string, Position>*>* animations;

	SphereNode* sphereNode1;
	SphereNode* sphereNode2;
	SphereNode* sphereNode3;

	std::list<BaseMeshPositionNode *> nodes;

	std::list<BaseLineNode *>* lines;
	float time;
	int num;
protected:
	Color IntToColor(int intColor);
	std::string FStringToStdString(FString fstring);
public:
	JsonGraphGame();
	~JsonGraphGame();

	void StartGreenLevel();
	void StartBitOfRedLevel();
	void StartYellowLevel();
	void StartRedLevel();
	void UpdatePosition(float time);
	void UpdateView(float DeltaTime) override;
	void StartGame() override;
};
