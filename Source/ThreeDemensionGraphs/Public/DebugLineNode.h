// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseNode.h"
#include "CoreTypes.h"
#include "BaseLineNode.h"

/**
 * 
 */
class DebugLineNode : public BaseLineNode
{
protected:
	TSubclassOf<AGraphLineNode> GetNode() override;
};
