// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraphBaseNode.h"
#include "GraphMeshNode.generated.h"

class UStaticMeshComponent;

/**
 * 
 */
UCLASS()
class THREEDEMENSIONGRAPHS_API AGraphMeshNode : public AGraphBaseNode
{
	GENERATED_BODY()

public:

	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Params")
	float size;

	AGraphMeshNode();
    
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Custom Event")
    void OnChangeColorEvent();
    virtual void OnChangeColorEvent_Implementation();
    
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Custom Event")
    void OnChangePositionEvent(FVector Position);
	virtual void OnChangePositionEvent_Implementation(FVector Position);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Custom Event")
	void OnChangeSizeEvent();
	virtual void OnChangeSizeEvent_Implementation();

};
