#ifndef CoreTypes_h
#define CoreTypes_h

struct Position {
    float x;
    float y;
    float z;
	Position() {};
	Position(float x, float y, float z) : x(x), y(y), z(z) {};

	Position operator + (Position b) {
		return Position(this->x + b.x, this->y + b.y, this->z + b.z);
	};
	Position operator - (Position b) {
		return Position(this->x - b.x, this->y - b.y, this->z - b.z);
	};
	Position operator * (float s) {
		return Position(s * this->x, s * this->y, s * this->z);
	}

};


struct Color
{
	float r;
	float g;
	float b;
	float a;
};

#endif /* CoreTypes_h */
