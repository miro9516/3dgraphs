// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseNode.h"

/**
 * 
 */
class ButtonsActions
{
public:
	ButtonsActions();
	~ButtonsActions();
    
    virtual void OnAction1(BaseNode* InteractedNode);
    virtual void OnAction2(BaseNode* InteractedNode);
    virtual void OnAction3(BaseNode* InteractedNode);
	virtual void OnAction4(BaseNode* InteractedNode);
    virtual void OnAction5(BaseNode* InteractedNode);
    virtual void OnAction6(BaseNode* InteractedNode);
    virtual void OnPress0();
    virtual void OnPress9();
};
