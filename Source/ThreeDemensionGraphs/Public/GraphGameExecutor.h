// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraphGameInstance.h"
class GraphGameInstance;

/**
 * 
 */
class GraphGameExecutor
{
public:
	GraphGameExecutor();
	~GraphGameExecutor();

	GraphGameInstance* GetGameInstance();
};
