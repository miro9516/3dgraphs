// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseNode.h"
#include "ObjectMatcherActorComponent.h"
class UObjectMatcherActorComponent;
class BaseNode;
class ButtonsActions;

/**
 * 
 */
class GraphGameInstance
{
private:
    UObjectMatcherActorComponent* ObjectMatcherActorComponent;
    ButtonsActions* GameButtonsActions = nullptr;
protected:
    void SetButtonsActions(ButtonsActions* NewGameButtonsActions);

	float GetCurrentTime();
public:
    
	GraphGameInstance();
	virtual ~GraphGameInstance() = 0;

	virtual void StartGame();

	virtual void UpdateView(float DeltaTime);
    
    ButtonsActions* GetButtonsActions();
    
    void SetObjectMatcherActorComponent(UObjectMatcherActorComponent* NewObjectMatcherActorComponent);
};
