
#pragma once

#include "CoreMinimal.h"
#include "ButtonsActions.h"
#include "JsonGraphGame.h"
#include "BaseNode.h"
class ButtonsActions;
class JsonGraphGame;

/**
 * 
 */
class JsonButtonsActions : public ButtonsActions
{
public:
	JsonButtonsActions();
	~JsonButtonsActions();
    
    void OnAction1(BaseNode* InteractedNode) override;
    void OnAction2(BaseNode* InteractedNode) override;
    void OnAction3(BaseNode* InteractedNode) override;
    void OnAction4(BaseNode* InteractedNode) override;
    void OnAction5(BaseNode* InteractedNode) override;
    void OnAction6(BaseNode* InteractedNode) override;
    void OnPress0() override;
    void OnPress9() override;
    
    JsonGraphGame* GraphGame = nullptr;
};
