// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CoreTypes.h"
#include "BaseMeshPositionNode.h"

/**
 * 
 */
class ComputerNode : public BaseMeshPositionNode
{
protected:
	TSubclassOf<AGraphMeshNode> GetMesh() override;
};
