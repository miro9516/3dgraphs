// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseLineNode.h"

/**
 * 
 */
class ConnectionLineNode : public BaseLineNode
{
private:
	bool TwoWayConnection;

protected:
	TSubclassOf<AGraphLineNode> GetNode() override;
	AGraphLineNode* SpawnActor() override;

public:

	void setTwoWayConnection(bool TwoWayConnection);
};
