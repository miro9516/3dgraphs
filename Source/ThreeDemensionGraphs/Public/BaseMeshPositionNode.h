// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

using namespace std;

#include "CoreMinimal.h"
#include "BaseNode.h"
#include <unordered_set>
#include "BaseLineNode.h"
class BaseLineNode;
/**
 * 
 */
class BaseMeshPositionNode : public BaseNode
{

private:
	Color color;
	Position position;
	float size = 1.;
	std::unordered_set<BaseLineNode*>* baseLineNodes = nullptr;
protected:
	virtual TSubclassOf<AGraphMeshNode> GetMesh();
public:
	BaseMeshPositionNode();

	void Spawn() override;
	void Delete() override;

	void SetPosition(Position newPosition);
	void SetColor(Color newColor);
	void SetSize(float size);
	float GetSize();
	Position GetPosition();
	Color GetColor();

	void AddLineNode(BaseLineNode* LineNode);
	void DeleteLineNode(BaseLineNode* LineNode);
	std::unordered_set<BaseLineNode*> GetLineNodes();
};
