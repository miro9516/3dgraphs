// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraphGameInstance.h"
#include "GraphBaseNode.h"
#include "ButtonsPanel.generated.h"

/**
 * 
 */
UCLASS(ClassGroup=(Custom))
class UButtonsPanel : public UObject
{
    GENERATED_BODY()
private:
    GraphGameInstance* GameInstance = nullptr;
    
public:
	UButtonsPanel();
    
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void action1(AGraphBaseNode* node);
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void action2(AGraphBaseNode* node);
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void action3(AGraphBaseNode* node);
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void action4(AGraphBaseNode* node);
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void action5(AGraphBaseNode* node);
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void action6(AGraphBaseNode* node);
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void onPress0();
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void onPress9();
    
    void SetGameInstance(GraphGameInstance* NewGameInstance);
};
