// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraphBaseNode.h"
#include "GraphLineNode.generated.h"


/**
 * 
 */
UCLASS()
class THREEDEMENSIONGRAPHS_API AGraphLineNode : public AGraphBaseNode
{
	GENERATED_BODY()

protected:


public:

    AGraphLineNode();

	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Params")
	FVector startPosition;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Params")
	FVector endPosition;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Params")
	bool twoDirectional;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Params")
	float thickness;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Custom Event")
	void OnChangeThicknessEvent();
	virtual void OnChangeThicknessEvent_Implementation();
    
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Custom Event")
    void OnChangePositionEvent();
	virtual void OnChangePositionEvent_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Custom Event")
	void OnChangeColorEvent();
	virtual void OnChangeColorEvent_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Custom Event")
	void OnChangeDirectionEvent();
	virtual void OnChangeDirectionEvent_Implementation();
};
