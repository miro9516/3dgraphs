// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CoreTypes.h"
#include <string>
#include "GraphGameExecutor.h"
#include "GraphBaseNode.h"
#include "ObjectMatcherActorComponent.generated.h"

using namespace std;
class AGraphMeshNode;
class AGraphLineNode;
class AGraphBaseNode;
class GraphGameInstance;
class UButtonsPanel;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THREEDEMENSIONGRAPHS_API UObjectMatcherActorComponent : public UActorComponent
{
	GENERATED_BODY()

private:
	GraphGameInstance* GameInstance; 

	FTimerHandle TimerBetweenUpdates;
	TArray<AActor*>* ActorsForUpdate;
	int32 chunkUpdateActors = 0;
	float timeBetweenUpdates = 2.;
	int32 maxActorsForUpdate = 300;

    
    UButtonsPanel* ButtonsPanel = nullptr;

	void PartialActorsUpdate();
public:	
	// Sets default values for this component's properties
	UObjectMatcherActorComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Update Actors")
	void UpdateActors();
	
	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> sphereNode; // 0

	// ���� ��� ������� ���� ����� ����

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> mobilePhoneNode; // 1

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> computerNode; // 2

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> notebookNode; // 3

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> firewallNode; // 4

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> gatewayNode; // 5

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> networkFirewall; // 6

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> serverStorageNode; // 7

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> databaseServerNode; // 8

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> routerWithFirewallNode; // 9

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> switchLayer1Node; // 10

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> switchLayer3Node; // 11

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> hubNode; // 12

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> router; // 13

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> internetNode; // 14

	// /���� ��� ������� ���� ����� ����

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphLineNode> debugLineNode;

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphLineNode> connectionLineNode;
    
    UFUNCTION(BlueprintCallable, Category = "Buttons Panel")
    UButtonsPanel* GetButtonsPanel();
};
