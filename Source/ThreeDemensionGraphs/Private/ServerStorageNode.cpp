// Fill out your copyright notice in the Description page of Project Settings.

#include "ServerStorageNode.h"

TSubclassOf<AGraphMeshNode> ServerStorageNode::GetMesh()
{
	return component->serverStorageNode;
}
