// Fill out your copyright notice in the Description page of Project Settings.

#include "ButtonsPanel.h"
#include "ButtonsActions.h"
#include "GraphGameInstance.h"
#include "GraphBaseNode.h"
class ButtonsActions;
class GraphGameInstance;

UButtonsPanel::UButtonsPanel()
{
}

void UButtonsPanel::action1(AGraphBaseNode* node)
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions && node) {
        Actions->OnAction1(node->GetBaseNode());
    }
}

void UButtonsPanel::action2(AGraphBaseNode* node)
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions && node) {
        Actions->OnAction2(node->GetBaseNode());
    }
}

void UButtonsPanel::action3(AGraphBaseNode* node)
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions && node) {
        Actions->OnAction3(node->GetBaseNode());
    }
}

void UButtonsPanel::action4(AGraphBaseNode* node)
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions && node) {
        Actions->OnAction4(node->GetBaseNode());
    }
}

void UButtonsPanel::action5(AGraphBaseNode* node)
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions && node) {
        Actions->OnAction5(node->GetBaseNode());
    }
}

void UButtonsPanel::action6(AGraphBaseNode* node)
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions && node) {
        Actions->OnAction6(node->GetBaseNode());
    }
}

void UButtonsPanel::onPress0()
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions) {
        Actions->OnPress0();
    }
}

void UButtonsPanel::onPress9()
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions) {
        Actions->OnPress9();
    }
}

void UButtonsPanel::SetGameInstance(GraphGameInstance* NewGameInstance)
{
    GameInstance = NewGameInstance;
}
