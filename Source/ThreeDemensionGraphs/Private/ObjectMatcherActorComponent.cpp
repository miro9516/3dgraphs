// Fill out your copyright notice in the Description page of Project Settings.

#include "ObjectMatcherActorComponent.h"
#include "BaseNode.h"
#include "GraphBaseNode.h"
#include "ButtonsPanel.h"
#include "GameFramework/GameNetworkManager.h"
#include "UObjectGlobals.h"
#include "Class.h"
#include "Components/ActorComponent.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"

class BaseNode;
class SphereNode;
class FJsonObject;
class AGraphBaseNode;
class UButtonsPanel;


// Sets default values for this component's properties
UObjectMatcherActorComponent::UObjectMatcherActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;	

	GraphGameExecutor* GameExecutor = new GraphGameExecutor();
    
    ButtonsPanel = NewObject<UButtonsPanel>();

	GameInstance = GameExecutor->GetGameInstance();
    
    ButtonsPanel->SetGameInstance(GameInstance);
	ButtonsPanel->AddToRoot();
	// ...
}

// Called when the game starts
void UObjectMatcherActorComponent::BeginPlay()
{
	Super::BeginPlay();

	BaseNode::component = this;
    
    GameInstance->SetObjectMatcherActorComponent(this);
	GameInstance->StartGame();
}

// Called every frame
void UObjectMatcherActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	GameInstance->UpdateView(DeltaTime);
}

UButtonsPanel* UObjectMatcherActorComponent::GetButtonsPanel()
{
    return ButtonsPanel;
}

void UObjectMatcherActorComponent::UpdateActors()
{
	const UWorld* world = GetWorld();
	ActorsForUpdate = new TArray<AActor*>();
	UGameplayStatics::GetAllActorsOfClass((const UObject*)world, AGraphBaseNode::StaticClass(), *ActorsForUpdate);
	chunkUpdateActors = 0;
	
	GetWorld()->GetTimerManager().SetTimer(TimerBetweenUpdates, this, &UObjectMatcherActorComponent::PartialActorsUpdate, timeBetweenUpdates, true, 0.);
}


void UObjectMatcherActorComponent::PartialActorsUpdate()
{
	int32 max = (chunkUpdateActors + 1) * maxActorsForUpdate;
	int32 size = ActorsForUpdate->Num();
	if (max > size)
	{
		max = size;
	}

	for (int32 i = chunkUpdateActors * maxActorsForUpdate; i < max; i++)
	{
		AActor* actor = (*ActorsForUpdate)[i];
		AGraphBaseNode* baseNodeActor = (AGraphBaseNode*)actor;
		baseNodeActor->OnUpdateOnClientEvent();
	}

	if (max == size)
	{
		(&TimerBetweenUpdates)->Invalidate();
	}
	else
	{
		chunkUpdateActors++;
	}
}