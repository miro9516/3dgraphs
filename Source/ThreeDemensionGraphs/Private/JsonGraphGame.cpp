// Fill out your copyright notice in the Description page of Project Settings.

#include "JsonGraphGame.h"
#include "SphereNode.h"
#include "DebugLineNode.h"
#include "Json.h"
#include "Paths.h"
#include "FileHelper.h"
#include "JsonButtonsActions.h"
#include <unordered_map>
#include "BaseMeshPositionNode.h"
#include "ConnectionLineNode.h"
#include "ComputerNode.h"
#include "NotebookNode.h"
#include "RouterNode.h"
#include "ServerStorageNode.h"
#include <math.h>
#include "FirewallNode.h"
#include "DatabaseServerNode.h"
#include "InternetNode.h"
#include <windows.h>
#include <cassert>

//#include "OpenVRExpansionFunctionLibrary.h"
#include "Engine/Engine.h"

DECLARE_LOG_CATEGORY_EXTERN(ServerLogs, Log, All);
DEFINE_LOG_CATEGORY(ServerLogs)

JsonGraphGame* game;



Position getBezierPoint(Position* points, int numPoints, float t) {
	Position* tmp = new Position[numPoints];
	memcpy(tmp, points, numPoints * sizeof(Position));
	int i = numPoints - 1;
	while (i > 0) {
		for (int k = 0; k < i; k++)
			tmp[k] = tmp[k] + (tmp[k + 1] - tmp[k]) * t;
		i--;
	}
	Position answer = tmp[0];
	delete[] tmp;
	return answer;
}


LRESULT CALLBACK LowLevelKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	BOOL fEatKeystroke = FALSE;

	if (nCode == HC_ACTION)
	{
		KBDLLHOOKSTRUCT  a = *(KBDLLHOOKSTRUCT*)lParam;

		switch (wParam)
		{
		case WM_KEYDOWN:
			break;
		case WM_SYSKEYDOWN:
			break;
		case WM_KEYUP:

			if (a.vkCode == 0x30)
			{
			//	game->StartGreenLevel();
			}
			else if (a.vkCode == 0x39)
			{
				game->StartRedLevel();
			}
			//else if (a.vkCode == 0x30)
			//{
			//	game->StartGreenLevel();
			//}
			break;
		case WM_SYSKEYUP:
			break;
		}
	}

	return(fEatKeystroke ? 1 : CallNextHookEx(NULL, nCode, wParam, lParam));
}



void JsonGraphGame::StartGreenLevel()
{
	//UE_LOG(ServerLogs, Display, TEXT("Start green level"));

	/*Color green;
	green.r = 0;
	green.g = 255;
	green.b = 0;
	green.a = 1;

	for (auto cur_map : *IdToNodeMapping)
	{
		BaseMeshPositionNode* cur_node = (&cur_map)->second;
		cur_node->SetColor(green);
	}*/

}

void JsonGraphGame::StartBitOfRedLevel()
{

}

void JsonGraphGame::StartRedLevel()
{
	time = 0;
	num = 1;
}

void JsonGraphGame::StartYellowLevel()
{

}
 
JsonGraphGame::JsonGraphGame()
{
    JsonButtonsActions* Actions = new JsonButtonsActions();
    Actions->GraphGame = this;
    SetButtonsActions(Actions);
	IdToNodeMapping = new std::unordered_map<std::string, BaseMeshPositionNode *>();
	animations = new TArray<std::unordered_map<std::string, Position>*>;

	lines = new std::list<BaseLineNode*>();
	game = this;
}

JsonGraphGame::~JsonGraphGame()
{
}

void JsonGraphGame::UpdatePosition(float time)
{
	for (auto cur : *IdToNodeMapping)
	{
		BaseMeshPositionNode* meshNode = cur.second;
		std::string id = cur.first;

		Position posArray[100];

		for (int i = 0; i < 99; i++)
			posArray[i] = (*(*animations)[i])[id];

		Position pos = getBezierPoint(posArray, 100, time);

		meshNode->SetPosition(pos);
	}
}

void JsonGraphGame::UpdateView(float DeltaTime)
{
	if (num < 0)
		return;
	time += DeltaTime;
	UpdatePosition(time/TICK_TIME);

	/*if (time > TICK_TIME)
	{
		time = 0;
		if (num<MAX_NUM-5)
			num+=1;
	}*/
}

void JsonGraphGame::StartGame()
{
	time = 0;
	num = -100;
	FString CompleteFilePath = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "TextDocument/Main.txt";

	FString json;
	FFileHelper::LoadFileToString(json, *CompleteFilePath);

	TSharedPtr<FJsonObject> JsonParsed = MakeShareable(new FJsonObject());
	TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create(json);
	if (FJsonSerializer::Deserialize(JsonReader, JsonParsed) && JsonParsed.IsValid())
	{
		TArray<TSharedPtr<FJsonValue>> vertex = JsonParsed->GetArrayField("vertex");
		TArray<TSharedPtr<FJsonValue>> edges = JsonParsed->GetArrayField("edges");
		TArray<TSharedPtr<FJsonValue>> frames = JsonParsed->GetArrayField("frames");

		for (int32 i = 0; i < vertex.Num(); i++)
		{
			const TSharedPtr<FJsonObject>& oneVertex = vertex[i]->AsObject();
			Position position;
			position.x = oneVertex->GetNumberField("x");
			position.y = oneVertex->GetNumberField("y");
			position.z = oneVertex->GetNumberField("z");
			float size = oneVertex->GetNumberField("size");
			const TSharedPtr<FJsonObject>& colorObject = oneVertex->GetObjectField("color");
			Color color;
			color.r = colorObject->GetIntegerField("r") / 255.;
			color.g = colorObject->GetIntegerField("g") / 255.;
			color.b = colorObject->GetIntegerField("b") / 255.;
			color.a = colorObject->GetIntegerField("a") / 255.;
			FString nativeTitle = oneVertex->GetStringField("title");
			std::string title = FStringToStdString(nativeTitle);
			FString nativeContent = oneVertex->GetStringField("content");
			std::string content = FStringToStdString(nativeContent);

			FString nativeId = oneVertex->GetStringField("id");
			std::string id = FStringToStdString(nativeId);

			int32 type = oneVertex->GetIntegerField("type");

			BaseMeshPositionNode* meshNode;
			meshNode = new SphereNode();
			meshNode->SetSize(size);
			meshNode->SetPosition(position);
			meshNode->SetTitle(title);
			meshNode->SetContent(content);
			meshNode->Spawn();
			meshNode->SetColor(color);

			IdToNodeMapping->insert(std::pair<std::string, BaseMeshPositionNode *>(id, meshNode));
		}

		for (int32 i = 0; i < edges.Num(); i++)
		{
			const TSharedPtr<FJsonObject>& oneEdge = edges[i]->AsObject();
			std::string id1 = FStringToStdString(oneEdge->GetStringField("id1"));
			std::string id2 = FStringToStdString(oneEdge->GetStringField("id2"));
			float thickness = oneEdge->GetNumberField("thickness");

			const TSharedPtr<FJsonObject>& colorObject = oneEdge->GetObjectField("color");
			Color color;
			color.r = colorObject->GetIntegerField("r") / 255.;
			color.g = colorObject->GetIntegerField("g") / 255.;
			color.b = colorObject->GetIntegerField("b") / 255.;
			color.a = colorObject->GetIntegerField("a") / 255.;
			std::string title = FStringToStdString(oneEdge->GetStringField("text"));

			int32 type = oneEdge->GetIntegerField("type");

			BaseLineNode* lineN;
			switch (type)
			{
			case 0:
			{
				DebugLineNode* debugLineN = new DebugLineNode();
				lineN = debugLineN;
			}
			break;
			default:
			{
				ConnectionLineNode* connectionLineN = new ConnectionLineNode();
				bool twoWayDirection = oneEdge->GetIntegerField("direction") == 0;
				connectionLineN->setTwoWayConnection(twoWayDirection);
				lineN = connectionLineN;
			}
			break;
			}

			lineN->SetNodes(IdToNodeMapping->at(id1), IdToNodeMapping->at(id2));
			lineN->SetTitle(title);
			lineN->SetContent(title);
			lineN->SetThickness(thickness);
			lineN->Spawn();
			lineN->SetColor(color);

		}
		
		for (int32 i = 0; i < frames.Num(); i++)
		{
			const auto oneFrame = frames[i]->AsObject()->GetArrayField("frame");
			std::unordered_map<std::string, Position>* animation = new std::unordered_map<std::string, Position>();
			for (int32 j = 0; j < oneFrame.Num(); j++)
			{
				auto oneVertex = oneFrame[j]->AsObject();

				FString nativeId = oneVertex->GetStringField("id");
				std::string id = FStringToStdString(nativeId);

				Position position;
				position.x = oneVertex->GetNumberField("x");
				position.y = oneVertex->GetNumberField("y");
				position.z = oneVertex->GetNumberField("z");

				animation->insert(std::pair<std::string, Position>(id, position));
			}
			animations->Add(animation);
		}
	}

	SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)LowLevelKeyboardProc, NULL, NULL);
}

Color JsonGraphGame::IntToColor(int intColor) {
	float mainColor = (intColor % 17) / 16.0f;
	Color color;
	color.r = 0;
	color.g = 0;
	color.b = 0;
	color.a = 1;
	switch (intColor % 3) {
	case 0:
		color.r = mainColor;
		break;
	case 1:
		color.g = mainColor;
		break;
	default:
		color.b = mainColor;
		break;
	}
	return color;
}

std::string JsonGraphGame::FStringToStdString(FString fstring) {
	return TCHAR_TO_UTF8(*fstring);
}

