// Fill out your copyright notice in the Description page of Project Settings.

#include "JsonButtonsActions.h"
#include "BaseNode.h"
#include "ConnectionLineNode.h"
#include "DebugLineNode.h"

JsonButtonsActions::JsonButtonsActions()
{
}

JsonButtonsActions::~JsonButtonsActions()
{
}

void JsonButtonsActions::OnAction1(BaseNode* InteractedNode)
{
	// otpravit' uvedomlenie
}

void JsonButtonsActions::OnAction2(BaseNode* InteractedNode)
{
	if (InteractedNode)
	{
		if (BaseMeshPositionNode* meshPositionNode = static_cast<BaseMeshPositionNode*>(InteractedNode))
		{
			Color darkBlue;
			darkBlue.r = 0;
			darkBlue.g = 0.025;
			darkBlue.b = 0.08;
			darkBlue.a = 1;

			meshPositionNode->SetColor(darkBlue);
			auto lineNodes = meshPositionNode->GetLineNodes();
			for (BaseLineNode* lineNode : lineNodes)
			{
				if (ConnectionLineNode* connectionLineNode = static_cast<ConnectionLineNode*>(lineNode))
				{
					DebugLineNode* newLineNode = new DebugLineNode();
					newLineNode->SetNodes(connectionLineNode->GetStartNode(), connectionLineNode->GetEndNode());
					newLineNode->SetColor(darkBlue);
					newLineNode->SetTitle(connectionLineNode->GetTitle());
					newLineNode->SetContent(connectionLineNode->GetContent());
					connectionLineNode->Delete();
					newLineNode->Spawn();
				}
				else 
				{
					lineNode->SetColor(darkBlue);
				}
			}
		}
	}
}

void JsonButtonsActions::OnAction3(BaseNode* InteractedNode)
{
    
}

void JsonButtonsActions::OnAction4(BaseNode* InteractedNode)
{
    
}

void JsonButtonsActions::OnAction5(BaseNode* InteractedNode)
{
    
}

void JsonButtonsActions::OnAction6(BaseNode* InteractedNode)
{
    
}

void JsonButtonsActions::OnPress0()
{
//    GraphGame->StartRedLevel();
}

void JsonButtonsActions::OnPress9()
{
//    GraphGame->StartGreenLevel();
}

