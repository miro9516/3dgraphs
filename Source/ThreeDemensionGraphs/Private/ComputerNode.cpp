// Fill out your copyright notice in the Description page of Project Settings.

#include "ComputerNode.h"

TSubclassOf<AGraphMeshNode> ComputerNode::GetMesh()
{
	return component->computerNode;
}
