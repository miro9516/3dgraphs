// Fill out your copyright notice in the Description page of Project Settings.

#include "FirewallNode.h"

TSubclassOf<AGraphMeshNode> FirewallNode::GetMesh()
{
	return component->firewallNode;
}
