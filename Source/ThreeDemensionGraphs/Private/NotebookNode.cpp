// Fill out your copyright notice in the Description page of Project Settings.

#include "NotebookNode.h"

TSubclassOf<AGraphMeshNode> NotebookNode::GetMesh()
{
	return component->notebookNode;
}
