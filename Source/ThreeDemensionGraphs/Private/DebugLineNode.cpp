// Fill out your copyright notice in the Description page of Project Settings.

#include "DebugLineNode.h"
#include "Kismet/GameplayStatics.h"
#include "GraphLineNode.h"


TSubclassOf<AGraphLineNode> DebugLineNode::GetNode()
{
	return component->debugLineNode;
}

