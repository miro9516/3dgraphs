// Fill out your copyright notice in the Description page of Project Settings.

#include "ConnectionLineNode.h"
#include "GraphLineNode.h"

TSubclassOf<AGraphLineNode> ConnectionLineNode::GetNode()
{
	return component->connectionLineNode;
}

AGraphLineNode* ConnectionLineNode::SpawnActor()
{
	AGraphLineNode* lineNode = BaseLineNode::SpawnActor();
	if (lineNode)
	{
		lineNode->twoDirectional = TwoWayConnection;
	}
    
	return lineNode;
}

void ConnectionLineNode::setTwoWayConnection(bool TwoWayConnection)
{
	this->TwoWayConnection = TwoWayConnection;
	if (graphBaseNode)
	{
		AGraphLineNode* graphLineNode = (AGraphLineNode*)graphBaseNode;
        graphLineNode->twoDirectional = TwoWayConnection;
        graphLineNode->OnChangeDirectionEvent();
	}
}
