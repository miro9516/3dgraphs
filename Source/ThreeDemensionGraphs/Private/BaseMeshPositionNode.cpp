// Fill out your copyright notice in the Description page of Project Settings.



#include "BaseMeshPositionNode.h"


#include "SphereNode.h"
#include "BaseLineNode.h"
#include "GraphMeshNode.h"
#include "ObjectMatcherActorComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/EngineTypes.h"
#include "CoreMinimal.h"
#include <unordered_set>
#include "GraphLineNode.h"
using namespace std;

void BaseMeshPositionNode::SetColor(Color newColor)
{
	color = newColor;
	if (graphBaseNode)
	{
		graphBaseNode->color = CastColorToFColor(newColor);
		((AGraphMeshNode *)graphBaseNode)->OnChangeColorEvent();
	}
}

void BaseMeshPositionNode::SetSize(float size)
{
	this->size = size;
	if (graphBaseNode)
	{
		AGraphMeshNode* meshNode = (AGraphMeshNode *)graphBaseNode;
		meshNode->size = size;
		meshNode->OnChangeSizeEvent();
	}
}

float BaseMeshPositionNode::GetSize()
{
	return size;
}

Position BaseMeshPositionNode::GetPosition()
{
	return position;
}

Color BaseMeshPositionNode::GetColor()
{
	return color;
}

void BaseMeshPositionNode::AddLineNode(BaseLineNode* LineNode)
{
	baseLineNodes->insert(LineNode);
}

void BaseMeshPositionNode::DeleteLineNode(BaseLineNode* LineNode)
{
	baseLineNodes->erase(LineNode);
}

std::unordered_set<BaseLineNode*> BaseMeshPositionNode::GetLineNodes()
{
	return *baseLineNodes;
}

TSubclassOf<AGraphMeshNode> BaseMeshPositionNode::GetMesh()
{
	return nullptr;
}

BaseMeshPositionNode::BaseMeshPositionNode()
{
	baseLineNodes = new std::unordered_set<BaseLineNode*>();
}

void BaseMeshPositionNode::Spawn()
{
	FTransform SpawnTransform;
	FVector SpawnLocation = CastPositionToFVector(position);
	SpawnTransform.SetLocation(SpawnLocation);
	SpawnTransform.SetRotation(FQuat(0, 0, 0, 0));
	SpawnTransform.SetScale3D(FVector(size, size, size));

	AGraphMeshNode* spawnNode = component->GetWorld()->SpawnActorDeferred<AGraphMeshNode>(GetMesh(), SpawnTransform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	if (spawnNode)
	{
		spawnNode->color = CastColorToFColor(color);
		spawnNode->size = size;
		spawnNode->SetBaseNode(this);
		UGameplayStatics::FinishSpawningActor(spawnNode, SpawnTransform);
		graphBaseNode = spawnNode;
	}
}

void BaseMeshPositionNode::Delete()
{
	std::unordered_set<BaseLineNode*> baseLineNodesCopy = *baseLineNodes;
	for (auto baseLineNode : baseLineNodesCopy)
	{
		baseLineNode->Delete();
	}
	baseLineNodes->clear();
	BaseNode::Delete();
}

void BaseMeshPositionNode::SetPosition(Position newPosition)
{
	position = newPosition;
	if (graphBaseNode) {
		AGraphMeshNode* graphMeshNode = (AGraphMeshNode*)graphBaseNode;
		graphMeshNode->OnChangePositionEvent(CastPositionToFVector(position));

		std::unordered_set<BaseLineNode*>::iterator it = baseLineNodes->begin();
		while (it != baseLineNodes->end())
		{
			AGraphLineNode* graphLineNode = (AGraphLineNode *)((*it)->graphBaseNode);
			if ((*it)->GetStartNode() == this)
			{
				graphLineNode->startPosition = CastPositionToFVector(position);
			} 
			else
			{
				graphLineNode->endPosition = CastPositionToFVector(position);
			}
			
			graphLineNode->OnChangePositionEvent();
			it++;
		}
	}
}
