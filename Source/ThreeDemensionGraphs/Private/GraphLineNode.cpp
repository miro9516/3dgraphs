// Fill out your copyright notice in the Description page of Project Settings.

#include "GraphLineNode.h"
#include "Classes/Components/CapsuleComponent.h"
#include "Net/UnrealNetwork.h"


AGraphLineNode::AGraphLineNode()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void AGraphLineNode::OnChangeThicknessEvent_Implementation(){}
void AGraphLineNode::OnChangePositionEvent_Implementation(){}
void AGraphLineNode::OnChangeColorEvent_Implementation(){}

void AGraphLineNode::OnChangeDirectionEvent_Implementation(){}

void AGraphLineNode::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGraphLineNode, startPosition);
	DOREPLIFETIME(AGraphLineNode, endPosition);
	DOREPLIFETIME(AGraphLineNode, thickness);
	DOREPLIFETIME(AGraphLineNode, twoDirectional);
}
