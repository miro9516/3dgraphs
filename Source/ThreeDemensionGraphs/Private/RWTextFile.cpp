// Fill out your copyright notice in the Description page of Project Settings.

#include "RWTextFile.h"

bool URWTextFile::LoadTxt(FString FileNameA, FString& SaveTextA)
{
	FString CompleteFilePath = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir());

	//FPaths::ConvertRelativePathToFull(FPaths::GameDir());
	CompleteFilePath += "TextDocument/";

	return FFileHelper::LoadFileToString(SaveTextA, *(CompleteFilePath + FileNameA));
}

bool URWTextFile::SaveTxt(FString SaveTextB, FString FileNameB)
{
	return FFileHelper::SaveStringToFile(SaveTextB, *(FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + FileNameB));
}


