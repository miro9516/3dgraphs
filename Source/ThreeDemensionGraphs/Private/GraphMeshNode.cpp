// Fill out your copyright notice in the Description page of Project Settings.

#include "GraphMeshNode.h"
#include "Net/UnrealNetwork.h"


AGraphMeshNode::AGraphMeshNode()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AGraphMeshNode::OnChangeColorEvent_Implementation(){}

void AGraphMeshNode::OnChangePositionEvent_Implementation(FVector Position)
{

}

void AGraphMeshNode::OnChangeSizeEvent_Implementation(){}

void AGraphMeshNode::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGraphMeshNode, size);
}