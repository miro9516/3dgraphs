// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseLineNode.h"
#include "GraphLineNode.h"
#include "BaseNode.h"
#include "Kismet/GameplayStatics.h"

TSubclassOf<AGraphLineNode> BaseLineNode::GetNode()
{
	return nullptr;
}

AGraphLineNode* BaseLineNode::SpawnActor()
{
	FTransform SpawnTransform;
	FVector SpawnLocation = CastPositionToFVector(NodeStart->GetPosition());
	SpawnTransform.SetLocation(SpawnLocation);
	SpawnTransform.SetRotation(FQuat(0, 0, 0, 0));
	SpawnTransform.SetScale3D(FVector(1, 1, 1));

	this->spawnTransform = SpawnTransform;

	AGraphLineNode* spawnNode = component->GetWorld()->SpawnActorDeferred<AGraphLineNode>(GetNode(), SpawnTransform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	if (spawnNode)
	{
		spawnNode->color = CastColorToFColor(color);
		spawnNode->startPosition = CastPositionToFVector(NodeStart->GetPosition());
		spawnNode->endPosition = CastPositionToFVector(NodeEnd->GetPosition());
		spawnNode->thickness = Thickness;
		spawnNode->SetBaseNode(this);
		
		graphBaseNode = spawnNode;
	}
	return spawnNode;
}

void BaseLineNode::FinishSpawnActor(AGraphLineNode* graphLineNode)
{
	UGameplayStatics::FinishSpawningActor(graphLineNode, this->spawnTransform);
}

BaseLineNode::BaseLineNode()
{
}

BaseLineNode::~BaseLineNode()
{
}

void BaseLineNode::Spawn()
{
	AGraphLineNode* graphLineNode = SpawnActor();
	if (graphLineNode)
	{
		FinishSpawnActor(graphLineNode);
	}
}

void BaseLineNode::Delete()
{
	NodeStart->DeleteLineNode(this);
	NodeEnd->DeleteLineNode(this);
	BaseNode::Delete();
}

void BaseLineNode::SetNodes(BaseMeshPositionNode* NewNodeStart, BaseMeshPositionNode* NewNodeEnd)
{
	NodeStart = NewNodeStart;
	NodeEnd = NewNodeEnd;

	NodeStart->AddLineNode(this);
	NodeEnd->AddLineNode(this);
}

BaseMeshPositionNode* BaseLineNode::GetStartNode()
{
	return NodeStart;
}

BaseMeshPositionNode* BaseLineNode::GetEndNode()
{
	return NodeEnd;
}

void BaseLineNode::SetColor(Color newColor)
{
	color = newColor;
	if (graphBaseNode)
	{
		AGraphLineNode* node = (AGraphLineNode*)graphBaseNode;
		node->color = CastColorToFColor(newColor);
		node->OnChangeColorEvent();
	}
}

void BaseLineNode::SetThickness(float newThickness)
{
	Thickness = newThickness;
	if (graphBaseNode)
	{
		AGraphLineNode* node = (AGraphLineNode*)graphBaseNode;
		node->thickness = Thickness;
		node->OnChangeThicknessEvent();
	}
}

Color BaseLineNode::GetColor()
{
    return color;
}
