// Fill out your copyright notice in the Description page of Project Settings.

#include "RouterNode.h"

TSubclassOf<AGraphMeshNode> RouterNode::GetMesh()
{
	return component->router;
}
