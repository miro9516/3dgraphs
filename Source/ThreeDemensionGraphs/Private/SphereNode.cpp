// Fill out your copyright notice in the Description page of Project Settings.

#include "SphereNode.h"

TSubclassOf<AGraphMeshNode> SphereNode::GetMesh()
{
	return component->sphereNode;
}